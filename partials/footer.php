        <footer class="site-footer">
            <div class="container">
                <div class="col-md-6 col-md-offset-1 text-right">
                    <p class="footer-text">Situs ini sedang dalam tahap penyempurnaan</p>
                </div>
                <div class="col-md-5 text-right">
                    <!--a href="#" class="go-top"><i class="fa fa-angle-up"></i></a-->
                    <p class="footer-text">Copyright © 2016 PT Amman Mineral Nusa Tenggara, All rights reserved</p>
                </div>
            </div>
        </footer>

        <script src="<?php echo $baseURL; ?>js/vendor/jquery-1.10.2.min.js"></script>
        <script src="<?php echo $baseURL; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $baseURL; ?>js/min/plugins.min.js"></script>
        <script src="<?php echo $baseURL; ?>js/min/main.min.js"></script>
		<script>
			autoPlayYouTubeModal();

			//FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
			function autoPlayYouTubeModal() {
			  var trigger = $("body").find('[data-toggle="modal"]');
			  trigger.click(function () {
				  var theModal = $(this).data("target"),
					  videoSRC = $(this).attr("data-theVideo"),
					  videoSRCauto = videoSRC + "?autoplay=1";
				  $(theModal + ' iframe').attr('src', videoSRCauto);
				  $(theModal + ' button.close').click(function () {
					  $(theModal + ' iframe').attr('src', videoSRC);
				  });
			  });
			}
			$(function() {
			    $('.navbar-toggle').on('click', function() {
			        $('body').toggleClass('menu-open');
			    })
			})
			$("#button").click(function(){
				$('html,body').animate({
					scrollDown:$("#about").offset().down
				},2000);
			});
					
		</script>
    </body>
</html>