<?php 
	$doc_root = preg_replace("!${_SERVER['SCRIPT_NAME']}$!", '', $_SERVER['SCRIPT_FILENAME']);
		// base directory
	$base_dir = __DIR__;

	// server protocol
	$protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';

	// domain name
	$domain = $_SERVER['SERVER_NAME'];

	// base url
	$base_url = preg_replace("!^${doc_root}!", '', $base_dir);

	// server port
	$port = $_SERVER['SERVER_PORT'];
	$disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";

	// put em all together to get the complete base URL
	$url = "${protocol}://${domain}${disp_port}${base_url}";

	$baseURL = str_replace('partials', '', $url);

	// CHECK ID OR EN
	$isID = strpos($_SERVER['REQUEST_URI'],'id') > 0;	
?>

<!DOCTYPE html>
<html> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AMMAN MINERAL</title>
        <meta name="author" content="AMMAN MINERAL">
        <meta name="description" content="PT Amman Mineral Nusa Tenggara (PT AMNT), sebelumnya adalah PT Newmont Nusa Tenggara, merupakan perusahaan tambang tembaga dan emas nasional di Indonesia yang berlokasi di Kabupaten Sumbawa Barat, Provinsi Nusa Tenggara Barat. Kami mengoperasikan tambang Batu Hijau berdasarkan Kontrak Karya Generasi IV yang ditandatangani pada 2 Desember 1986.">
		<link rel="shortcut icon" href="<?php echo $baseURL; ?>images/favicon.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo $baseURL; ?>css/normalize.css">
        <link rel="stylesheet" href="<?php echo $baseURL; ?>css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo $baseURL; ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $baseURL; ?>css/style.css">
        <link rel="stylesheet" href="<?php echo $baseURL; ?>css/additional.css">
        <script src="<?php echo $baseURL; ?>js/vendor/modernizr-2.6.2.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<script>
		  $(function() {
			$('a[href*="#"]:not([href="#"])').click(function() {
			  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
				  $('html, body').animate({
					scrollTop: target.offset().top
				  }, 1000);
				  return false;
				}
			  }
			});
		  });
		</script>
    </head>
    <body>

		<nav class="navbar navbar-default " role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button> 
			
			<div class="business-logo-align col-md-12" >
				<a href="index.php"><img class="img-responsive logo desktop" src="<?php echo $baseURL ?>/images/logo.png" alt="logo"/></a>
				<a href="index.php"><img class="img-responsive logo mobile" src="<?php echo $baseURL ?>/images/logo_amman_mobile.png" alt="logo"/></a>
			</div> 
			
		</div>
		
		<div class="navbar-collapse collapse">
			<div class=" col-xs-12 col-sm-12 col-md-8 pd-t-10" >
				<ul class="nav navbar-nav navbar-centerr">
					<li><a href="index.php" class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'index.php'){echo 'busi-btn-active'; }else { echo 'busi-btn'; } ?>"><?php echo ($isID) ? "Beranda" : "Home" ; ?></a></li>
					<li><a href="tentang-kami.php" class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'tentang-kami.php'){echo 'busi-btn-active'; }else { echo 'busi-btn'; } ?>"><?php echo ($isID) ? "Tentang Kami" : "About Us" ; ?></a></li>
					<li><a href="berita.php" class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'berita.php'){echo 'busi-btn-active'; }else { echo 'busi-btn'; } ?>"><?php echo ($isID) ? "Berita" : "News" ; ?></a></li>
					<li><a href="kontak.php" class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'kontak.php'){echo 'busi-btn-active'; }else { echo 'busi-btn'; } ?>"><?php echo ($isID) ? "Kontak" : "Contact" ; ?></a></li>
					<li><a href="rekanan-kerja.php" class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'rekanan-kerja.php'){echo 'busi-btn-active'; }else { echo 'busi-btn'; } ?>"><?php echo ($isID) ? "Rekanan Kerja" : "Partners" ; ?></a></li>
				</ul>
			</div>
			<div class=" col-xs-12 col-sm-12 col-md-8 pd-t-10 nav-pills pull-right" >
				<ul class="nav navbar-nav navbar-right">
					<li class="switcher">
						<a href="<?php echo $baseURL; ?>" class="<?php echo ($isID) ? "" : "selected" ; ?>">EN</a> | <a class="<?php echo ($isID) ? "selected" : "" ; ?>" href="<?php echo $baseURL."id"; ?>">ID</a>
					</li>
					<li>
						<a href="https://twitter.com/NewmontID" target="_blank" class="social-ico"><i class="fa fa-twitter"></i></a>
					</li>
					<li>
						<a href="https://www.instagram.com/newmontid/" target="_blank" class="social-ico"><i class="fa fa-instagram"></i></a>
					</li>
					<li>
						<a href="https://www.facebook.com/NewmontID/" target="_blank" class="social-ico"><i class="fa fa-facebook"></i></a>
					</li>
				</ul>
			</div>
			
		</div>
	</nav>