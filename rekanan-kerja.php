<?php include 'partials/header.php'; ?>
        <section id="berita" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-xs-12 animation-element slide-left">
                        <h2 class="page-title text-center">Partners</h2>
						<div class="spacer"><br></div>
						<div class="spacer"><br></div>
					</div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>


        <section id="berita2" class="page-section">
            <div class="container">
				<div class="spacer"><br></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="page-subtitle">The triumph of an industry is a contribution of various stakeholders, including our partners. We encourage you to participate in developing national economy as partners. Together, we demonstrate actual work for Indonesia.</p>
                        <p class="page-subtitle">
                        Please refer to this  <a href="https://apps.ptnnt.co.id/vendorcenter/">link</a> to access the Vendor Application.</p>
					</div>           
                </div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="page-subtitle"></p>
					</div>           
                </div>
				<div class="spacer"><br></div>
            </div>
        </section>

<?php include 'partials/footer.php'; ?>
