<?php include 'partials/header.php'; ?>
        <section id="tentang-kami" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-xs-12 animation-element slide-left">
                        <h2 class="page-title text-center">ABOUT US</h2>
						<p class="page-subtitle text-center">PT Amman Mineral Nusa Tenggara (AMNT) is an Indonesian mining company which operates the Batu Hijau project. It is one of the several most promising prospects in the 25,000-hectare copper and gold concession located in West Sumbawa Regency, West Nusa Tenggara Province. Batu Hijau project is the second largest copper and gold mine in Indonesia and a world class asset.</p>
						<div class="spacer"><br></div>
						<div class="spacer"><br></div>
					</div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>


        <section id="tentang-kami2" class="page-section">
            <div class="container">
				<div class="spacer"><br></div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 animation-element slide-left">                   
						<div class="spacer" style="margin: 10px;"></div>
						<p class="page-subtitle">Since commencing production in 2000, we have produced approximately 8 million ounces of gold and 3.6 million tonnes of copper, with an expected mine life until 2025 and followed by processing of long-term stockpiles until 2031. We are driven by a significant expansion potential through the development of the Elang deposit and the Nangka prospect, which are currently under assessment. </p>
						<p class="page-subtitle">In 2016 alone we produced an outstanding result of 216,000 tonnes of copper and 800,000 ounces of gold. We own complete facilities that include a large fleet of mining equipment, a processing plant with 120,000 tonnes capacity per day, an 112-megawatt coal-fired power plant, a port with the ferry terminal, air services, and an established townsite. </p>
						<p class="page-subtitle">Having full support from the shareholders, PT Amman Mineral Internasional and PT Pukuafu Indah, we are committed to carrying out the finest and sustainable operating procedure through the implementation of the most advanced green technology available.</p>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="images/bg_tentang_kami_2.png" class="img-responsive">
                    </div>              
                </div>
				<div class="spacer"><br></div>
						
            </div>
        </section>


        <section id="tentang-kami3" class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 animation-element slide-left">
						<div class="spacer"></div>
                        <h2 class="page-title text-center">MANAGEMENT</h2>
					</div>
                </div>
            </div>
        </section>
        <section id="tentang-kami4" class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 animation-element slide-left">
                        <h3 class="page-title">Board of Directors</h3>
						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>President Director</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Rachmat Makkasau</p></b> </td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Makkasau has been the President Director since March 3, 2016, bringing more than 20 years of experience in the mining industry at PT Newmont Nusa Tenggara and PT Freeport Indonesia. <br></p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Vice President Director</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Agoes Projosasmito</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Projosasmito is seasoned entrepreneur in the natural resources and financial sectors.</p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Director</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: David Alexander Gibbs</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Ramlie previously was President Director and CEO of PT Borneo Lumbung Energi & Metal Tbk (2011-2015). He began his career as an investment banker and subsequently a private equity professional.</p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Director</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Naveen Chandralal</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Chandralal has 20 years of experience in developing and operating mining, mineral processing, and integrated infrastructure business.</p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Director</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Alexander Ramlie</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Gibbs has more than 35 years of international mining and consulting experience, including gold, copper, uranium, diamonds, and coal.</p>

						<h3 class="page-title">Board of Commissioners</h3>
						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Commissioner</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Muhammad Lutfi</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Lutfi is a businessman and Former Trade Minister, Chairman Indonesia Investment Coordinating Board (BKPM), Ambassador to Japan and Micronesia Federation.</p>

					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 animation-element slide-right">
						<h3 class="page-title text-center">Shareholders</h3>
						<img src="images/bg_tentang_kami_4.png" class="img-responsive">
					</div>
                </div>
				<div class="spacer"></div>
            </div>
        </section>
        

<?php include 'partials/footer.php'; ?>
