<?php include 'partials/header.php'; ?>

        <section id="kontak" class="page-section">
				<div class="spacer"><br></div>
				<div class="spacer"><br></div>
				<div class="spacer"><br></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2>Jakarta Office</h2>
						<p><strong>Energy Building, 28 <sup>th</sup> Floor</strong></p>
						<p><strong>SCBD Lot 11A</strong></p>
						<p><strong>Jalan Jend. Sudirman Kav. 52-53</strong></p>
						<p><strong>Kebayoran Baru</strong></p>
						<p><strong>Jakarta 12190</strong></p>
						<p><strong>Phone: +62 21 5799 4600 (Hunting)</strong></p>	
						<p><strong>Fax: +62 21 576 1464</strong></p>
						<br>						
					</div>   
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2>Batu Hijau Office</h2>
						<p><strong>West Sumbawa Regency</strong></p>
						<p><strong>West Nusa Tenggara</strong></p>
						<p><strong>Phone: +62 372 6353 18</strong></p>	
						<p><strong>Fax: +62 372 6353 19</strong></p>                        
					</div>        
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2>Mataram Office</h2>
						<p><strong>Jalan Sriwijaya No. 258</strong></p>
						<p><strong>Mataram, NTB, Indonesia</strong></p>
						<p><strong>Phone:  +62 372 6363 18</strong></p>	
						<p><strong>Fax:  +62 372 6333 49</strong></p>     
					</div>   
                    <div class="col-md-6 col-sm-6 col-xs-12">
						<h2>Communications Department</h2>
						<p class="page-subtitle">Email: communications@amnt.co.id </p>
					</div>        
                </div>
				<div class="spacer"><br></div>
            </div>
        </section>

<?php include 'partials/footer.php'; ?>
