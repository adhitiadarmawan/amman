<!DOCTYPE html>
<html> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AMMAN MINERAL</title>
        <meta name="author" content="AMMAN MINERAL">
        <meta name="description" content="PT Amman Mineral Nusa Tenggara (PT AMNT), sebelumnya adalah PT Newmont Nusa Tenggara, merupakan perusahaan tambang tembaga dan emas nasional di Indonesia yang berlokasi di Kabupaten Sumbawa Barat, Provinsi Nusa Tenggara Barat. Kami mengoperasikan tambang Batu Hijau berdasarkan Kontrak Karya Generasi IV yang ditandatangani pada 2 Desember 1986.">
		<link rel="shortcut icon" href="images/favicon.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default " role="navigation">
    			<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button> 
					
					<div class="business-logo-align col-md-12" >
						<a href="index.php"><img class="img-responsive logo desktop" src="images/logo.png" alt="logo"/></a>
						<a href="index.php"><img class="img-responsive logo mobile" src="images/logo_amman_mobile.png" alt="logo"/></a>
					</div> 
					
				</div>
				
				<div class="navbar-collapse collapse">
					<div class=" col-xs-12 col-sm-12 col-md-8 pd-t-10" >
						<ul class="nav navbar-nav navbar-centerr">
							<li><a href="index.php" class="busi-btn">BERANDA</a></li>
							<li><a href="tentang-kami.php" class="busi-btn">TENTANG KAMI</a></li>
							<li><a href="berita.php" class="busi-btn">BERITA</a></li>
							<li><a href="kontak.php" class="busi-btn">KONTAK</a></li>
							<li><a href="rekanan-kerja.php" class="busi-btn-active">REKANAN KERJA</a></li>
						</ul>
					</div>
					<div class=" col-xs-12 col-sm-12 col-md-8 pd-t-10 nav-pills pull-right" >
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="https://twitter.com/NewmontID" target="_blank" class="social-ico"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://www.instagram.com/newmontid/" target="_blank" class="social-ico"><i class="fa fa-instagram"></i></a>
							</li>
							<li>
								<a href="https://www.facebook.com/NewmontID/" target="_blank" class="social-ico"><i class="fa fa-facebook"></i></a>
							</li>
						</ul>
					</div>
					
				</div>
			</nav>


        <section id="berita" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-xs-12 animation-element slide-left">
                        <h2 class="page-title text-center">REKANAN KERJA</h2>
						<div class="spacer"><br></div>
						<div class="spacer"><br></div>
					</div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>


        <section id="berita2" class="page-section">
            <div class="container">
				<div class="spacer"><br></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="page-subtitle">Keberhasilan sebuah industri merupakan kontribusi dari berbagai pemangku kepentingan, termasuk mitra usaha.</p>
                        <p class="page-subtitle">Kami mengajak Anda untuk berpartisipasi dalam pembangunan perekonomian nasional sebagai rekanan kerja. Bersama, kita tunjukkan kinerja nyata bagi masyarakat Sumbawa dan Indonesia.</p>
                        <p class="page-subtitle">Silakan klik tautan <a href="https://apps.ptnnt.co.id/vendorcenter/">berikut</a> untuk mengakses Aplikasi Vendor.</p>
					</div>           
                </div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="page-subtitle"></p>
					</div>           
                </div>
				<div class="spacer"><br></div>
            </div>
        </section>
        <footer class="site-footer">
            <div class="container">
                <div class="col-md-6 col-md-offset-1 text-right">
                    <p class="footer-text">Situs ini sedang dalam tahap penyempurnaan</p>
                </div>
                <div class="col-md-5 text-right">
                    <!--a href="#" class="go-top"><i class="fa fa-angle-up"></i></a-->
                    <p class="footer-text">Copyright © 2016 PT Amman Mineral Nusa Tenggara, All rights reserved</p>
                </div>
            </div>
        </footer>
		<script src="js/vendor/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/min/plugins.min.js"></script>
        <script src="js/min/main.min.js"></script>
        <script>
			$(function() {
			    $('.navbar-toggle').on('click', function() {
			        $('body').toggleClass('menu-open');
			    })
			})
		</script>
    </body>
</html>
