<?php include '../partials/header.php'; ?>

        <section id="berita" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-xs-12 animation-element slide-left">
                        <h2 class="page-title text-center">REKANAN KERJA</h2>
						<div class="spacer"><br></div>
						<div class="spacer"><br></div>
					</div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>


        <section id="berita2" class="page-section">
            <div class="container">
				<div class="spacer"><br></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="page-subtitle">Keberhasilan sebuah industri merupakan kontribusi dari berbagai pemangku kepentingan, termasuk mitra usaha.</p>
                        <p class="page-subtitle">Kami mengajak Anda untuk berpartisipasi dalam pembangunan perekonomian nasional sebagai rekanan kerja. Bersama, kita tunjukkan kinerja nyata bagi masyarakat Sumbawa dan Indonesia.</p>
                        <p class="page-subtitle">Silakan klik tautan <a href="https://apps.ptnnt.co.id/vendorcenter/">berikut</a> untuk mengakses Aplikasi Vendor.</p>
					</div>           
                </div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="page-subtitle"></p>
					</div>           
                </div>
				<div class="spacer"><br></div>
            </div>
        </section>

<?php include '../partials/footer.php'; ?>
