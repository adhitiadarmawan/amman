<?php include '../partials/header.php'; ?>


        <section id="berita" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-xs-12 animation-element slide-left">
                        <h2 class="page-title text-center">SIARAN PERS</h2>
						<p class="page-subtitle text-center">Akuisisi Tambang Batu Hijau oleh Perusahaan Nasional Selesai</p>
						<div class="spacer"><br></div>
						<div class="spacer"><br></div>
					</div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>


        <section id="berita2" class="page-section">
            <div class="container">
				<div class="spacer"><br></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="page-subtitle">Jakarta, 27 Oktober 2016 – PT Newmont Nusa Tenggara (PTNNT) hari ini mengumumkan bahwa proses transaksi pengambilalihan kepemilikan saham di PTNNT sebesar 82,2% oleh PT Amman Mineral Internasional (PTAMI) telah selesai dilakukan dengan lancar.</p>
                        <p class="page-subtitle">Dengan selesainya proses transaksi tersebut, pemilik saham tambang Batu Hijau dan aset-aset terkait lainnya kini sepenuhnya dimiliki oleh perusahaan nasional, yakni PT Amman Mineral Internasional (PTAMI) yang menguasai 82,2% kepemilikan saham dan PT Pukuafu Indah (PTFI) sebagai pemegang saham minoritas sebanyak 17,8%.</p>
                        <p class="page-subtitle">PTAMI adalah perusahaan Indonesia yang pemegang sahamnya termasuk AP Investment dan Medco Energi. Dalam proses transaksi pembelian saham PTNNT ini, PTAMI didukung oleh sebuah konsorsium perbankan Indonesia dan internasional.</p>
                        <p class="page-subtitle">"Sebagai perusahaan nasional, PTNNT akan segera mengganti namanya menjadi PT Amman Mineral Nusa Tenggara (PTAMNT),” kata Rachmat Makkasau, Presiden Direktur PTNNT. “Dengan nilai dan budaya yang telah kita bangun untuk bisnis ini, kami merasa optimis akan dapat meningkatkan serta mengembangkan rasa saling percaya dan pengertian akan masa depan yang lebih baik dan kokoh  dengan seluruh pemangku kepentingan, termasuk pemerintah Indonesia, mitra bisnis, dan masyarakat setempat."</p>
                        <p class="page-subtitle">Kepemilikan PTAMNT oleh PTAMI merupakan langkah maju bagi bangsa Indonesia dan PTAMNT senantiasa berharap untuk terus meningkatkan nilai tambah dari sumber daya alam Indonesia demi  keberhasilan pembangunan negara.</p>
                        <p class="page-subtitle">Rachmat menambahkan, "Dengan pengetahuan dan keahlian yang kami miliki sebagai perusahaan tambang nasional dengan reputasi yang baik dan visi jangka panjang, kami optimis bahwa Batu Hijau dapat tetap menjadi pemimpin dalam industri pertambangan melalui penerapan teknologi terkini dan praktek penambangan berwawasan kelestarian lingkungan."</p>
						<p class="page-subtitle text-center">*****</p>
						<p class="page-subtitle"><strong>Tentang PT Amman Mineral Nusa Tenggara</strong></p>
                        <p class="page-subtitle">PTAMNT, sebelumnya PTNNT, adalah perusahaan tambang tembaga dan emas yang beroperasi berdasarkan Kontrak Karya Generasi ke-4 yang ditandatangani pada 2 Desember 1986. Sejak memulai kegiatan operasi secara penuh di Indonesia pada tahun 2000, perusahaan telah berkontribusi lebih dari Rp100 triliun dalam bentuk pembayaran pajak dan non pajak, royalti, gaji, pembelian barang dan jasa dalam negeri, serta dividen yang dibayarkan kepada para pemegang saham nasional. Selain itu, perusahaan juga telah menjalankan program tanggung jawab sosial untuk meningkatkan kualitas hidup dan kesejahteraan masyarakat sekitar dengan memberikan dana setiap tahun rata-rata sebesar Rp50 miliar. PTAMNT saat ini mempekerjakan sekitar 4,000 karyawan dan 3,500 kontraktor.</p>
						<p class="page-subtitle text-center">*****</p>
					</div>           
                </div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table border="none">
							<tr>
								<td><p class="page-subtitle">Kontak Media</p></td>
								<td></td>
							</tr>
							<tr>
								<td><p class="page-subtitle">Rubi W. Purnomo</p></td>
								<td><p class="page-subtitle">&ensp; : +62 811 940 399</p></td>
							</tr>
							<tr>
								<td><p class="page-subtitle">Email</p></td>
								<td><p class="page-subtitle">&ensp; : rubi.purnomo@nnt.co.id / rubi.purnomo@amnt.co.id</p></td>
							</tr>
						</table>
                    </div>           
                </div>
				<div class="spacer"><br></div>
            </div>
        </section>

<?php include '../partials/footer.php'; ?>
