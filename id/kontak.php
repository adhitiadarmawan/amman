<?php include '../partials/header.php'; ?>

        <section id="kontak" class="page-section">
				<div class="spacer"><br></div>
				<div class="spacer"><br></div>
				<div class="spacer"><br></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2>Kantor Jakarta</h2>
						<p class="page-subtitle"><strong>Menara Rajawali, Lantai 26</strong></p>
						<p class="page-subtitle"><strong>Jalan Mega Kuningan Lot 5.1</strong></p>
						<p class="page-subtitle"><strong>Kawasan Mega Kuningan</strong></p>
						<p class="page-subtitle"><strong>Jakarta 12950</strong></p>
					</div>   
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2>Kantor Site Batu Hijau</h2>
						<p class="page-subtitle"><strong>Sumbawa Barat, NTB, Indonesia</strong></p>
						<p class="page-subtitle"><span class="kontak-ico"> <i class="fa fa-phone"></i></span> +62 372 6353 18</p>
						<p class="page-subtitle"><span class="kontak-ico"> <i class="fa fa-facebook"></i></span> +62 372 6353 19</p>                        
					</div>        
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2>Kantor Mataram</h2>
						<p class="page-subtitle"><strong>Jalan Sriwijaya No. 258</strong></p>
						<p class="page-subtitle"><strong>Mataram, NTB, Indonesia</strong></p>
						<p class="page-subtitle"><span class="kontak-ico"> <i class="fa fa-phone"></i></span> +62 372 6363 18</p>
						<p class="page-subtitle"><span class="kontak-ico"> <i class="fa fa-facebook"></i></span> +62 372 6333 49</p>
					</div>   
                    <div class="col-md-6 col-sm-6 col-xs-12">
						<h2>Departemen Komunikasi PTAMNT</h2>
						<p class="page-subtitle"><span class="kontak-ico"> <i class="fa fa-envelope"></i></span> communications@amnt.co.id </p>
					</div>        
                </div>
				<div class="spacer"><br></div>
            </div>
        </section>
        
<?php include '../partials/footer.php'; ?>
