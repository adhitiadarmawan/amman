<?php include '../partials/header.php'; ?>

        <section id="tentang-kami" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-xs-12 animation-element slide-left">
                        <h2 class="page-title text-center">TENTANG KAMI</h2>
						<p class="page-subtitle text-center">PT Amman Mineral Nusa Tenggara (PT AMNT), sebelumnya adalah PT Newmont Nusa Tenggara, merupakan perusahaan tambang tembaga dan emas nasional di Indonesia yang berlokasi di Kabupaten Sumbawa Barat, Provinsi Nusa Tenggara Barat. Kami mengoperasikan tambang Batu Hijau berdasarkan Kontrak Karya Generasi IV yang ditandatangani pada 2 Desember 1986.</p>
						<div class="spacer"><br></div>
						<div class="spacer"><br></div>
					</div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>


        <section id="tentang-kami2" class="page-section">
            <div class="container">
				<div class="spacer"><br></div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 animation-element slide-left">                   
						<div class="spacer"><br></div>
						<p class="page-subtitle">Mendapat dukungan penuh dari pemilik saham, PT Amman Mineral Internasional dan PT Pukuafu Indah, kami berkomitmen untuk menyelenggarakan praktik tambang terbaik yang berkelanjutan melalui penerapan teknologi tertinggi dan berwawasan lingkungan.</p>
						<p class="page-subtitle">Kontribusi kami sejak pertama kali beroperasi pada tahun 2000 mencapai lebih dari Rp100 triliun dalam bentuk pembayaran pajak dan non pajak, royalti, gaji karyawan, pembelian barang dan jasa dalam negeri, serta dividen bagi para pemegang saham. Selain mempekerjakan sekitar 4.000 karyawan dan 4.000 kontraktor, kami turut mengambil bagian terhadap pemberdayaan masyarakat di sekitar tambang melalui penyediaan dana sebesar rerata Rp50 miliar per tahun.</p>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="../images/bg_tentang_kami_2.png" class="img-responsive">
                    </div>              
                </div>
				<div class="spacer"><br></div>
						
            </div>
        </section>


        <section id="tentang-kami3" class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 animation-element slide-left">
						<div class="spacer"></div>
                        <h2 class="page-title text-center">MANAJEMEN</h2>
					</div>
                </div>
            </div>
        </section>
        <section id="tentang-kami4" class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 animation-element slide-left">
                        <h3 class="page-title">Board of Directors</h3>
						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Presiden Direktur</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Rachmat Makkasau</p></b> </td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Makkasau has been the President Director since March 3, 2016, bringing more than 20 years of experience in the mining industry at PT Newmont Nusa Tenggara and PT Freeport Indonesia. <br></p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Wakil Presiden Direktur</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Agoes Projosasmito</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Projosasmito is seasoned entrepreneur in the natural resources and financial sectors.</p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Direktur</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: David Alexander Gibbs</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Ramlie previously was President Director and CEO of PT Borneo Lumbung Energi & Metal Tbk (2011-2015). He began his career as an investment banker and subsequently a private equity professional.</p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Direktur</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Naveen Chandralal</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Chandralal has 20 years of experience in developing and operating mining, mineral processing, and integrated infrastructure business.</p>

						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Direktur</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Alexander Ramlie</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Gibbs has more than 35 years of international mining and consulting experience, including gold, copper, uranium, diamonds, and coal.</p>

						<h3 class="page-title">Board of Commissioners</h3>
						<table border="none">
							<tr>
								<td><p class="page-subtitle no-padding"><b>Komisioner</b></p></td>
								<td><p class="page-subtitle no-padding"><b>: Muhammad Lutfi</b></p></td>
							</tr>
						</table>
						<p class="p-bottom-15">Mr. Lutfi is a businessman and Former Trade Minister, Chairman Indonesia Investment Coordinating Board (BKPM), Ambassador to Japan and Micronesia Federation.</p>

					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 animation-element slide-right">
						<h3 class="page-title text-center">Shareholders</h3>
						<img src="../images/bg_tentang_kami_4.png" class="img-responsive">
					</div>
                </div>
				<div class="spacer"></div>
            </div>
        </section>
        

<?php include '../partials/footer.php'; ?>
