<?php include 'partials/header.php'; ?>

        <section id="berita" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-xs-12 animation-element slide-left">
                        <h2 class="page-title text-center">Press Release</h2>
						<!-- <p class="page-subtitle text-center">Akuisisi Tambang Batu Hijau oleh Perusahaan Nasional Selesai</p> -->
						<div class="spacer"><br></div>
						<div class="spacer"><br></div>
					</div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>


        <section id="berita2" class="page-section">
            <div class="container">
				<div class="spacer"><br></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                    	<h2 class="page-title" style="padding-top: 0; padding-bottom: 20px">Acquisition of Batu Hijau Mine Completed by National Companies</h2>

                        <p class="page-subtitle">Jakarta, November 2, 2016 – PT Newmont Nusa Tenggara (PTNNT) is today announcing that the
acquisition by PT Amman Mineral Internasional (PTAMI) of 82.2% of the shares in PTNNT has been successfully completed. </p>
                        <p class="page-subtitle">As a result of the successful completion of the sale, the Batu Hijau mine and related assets are now fully owned by Indonesian companies, namely PT Amman Mineral Internasional (PTAMI) controlling 82.2% and PT Pukuafu Indah (PTPI) controlling the remaining 17.8%. </p>

                        <p class="page-subtitle">PTAMI is an Indonesian company whose shareholders are AP Investment and Medco Energi. PTAMI was supported by a consortium of Indonesian and international banks in concluding this purchase.</p>

                        <p class="page-subtitle">“As a national company, PTNNT will immediately change its name to PT Amman Mineral Nusa Tenggara (AMNT)”, said Rachmat Makkasau, President Director of PTNNT. “Building off the values and culture we have created for the business, we are optimistic that we will be able to further develop the trust and a shared understanding of the future with all our valued stakeholders, including the Indonesian government, business partners and local communities”.</p>

                        <p class="page-subtitle">The ownership of AMNT by PTAMI is a step forward for Indonesia and AMNT looks forward to continuing its contributions to the value added from Indonesia’s natural resources for the country development purposes.</p>

                        <p class="page-subtitle">Rachmat added, “With the knowledge and expertise we have as a reputable national mining company with a long-term vision, we are optimistic that Batu Hijau will continue to be a leader of global significance in Indonesia’s mining industry through the application of state-of-the-art technology and environmentally- responsible mining.”</p>

                        <p class="page-subtitle text-center">*****</p>

                        <h2 class="page-title" style="padding-top: 0; padding-bottom: 20px">AMNT Receives Extension of a Concentrate Export Permit </h2>

                        <p class="page-subtitle">Jakarta, February 24, 2017 - PT Amman Mineral Nusa Tenggara (AMNT) has been granted an extension of concentrate export permit from the Government. The export recommendation was issued by the Ministry of Energy and Mineral Resources on February 17, 2017 and the Ministry of Trade then issued a copper concentrate export permit with maximum volume of 675,000 WMT.</p>

                        <p class="page-subtitle">The company appreciates the cooperation between AMNT and the Government through the Ministry of Energy and Mineral Resources and the Ministry of Trade completing the recommendation application process and the extension of the Batu Hijau concentrate export permit. </p>

                        <p class="page-subtitle">“AMNT will soon resume its export activities and continue its Batu Hijau mining operations normally,” said Rachmat Makkasau, President Director of AMNT.</p>

                        <p class="page-subtitle">“We also look forward to continuously working together with the Government in achieving our shared goals namely sustainable mining operations and increasing the added value of AMNT mine as one of the domestic economic development stimuli in the long run as we have conveyed to the central and regional governments particularly in respect of their support to facilitate and support our future investment,” added Rachmat Makkasau.  </p>

						<p class="page-subtitle text-center">*****</p>
						<p class="page-subtitle"><strong>On PT Amman Mineral Nusa Tenggara </strong></p>
                        <p class="page-subtitle">AMNT, previously PTNNT, is a copper and gold mining company operating under the Generation IV Contract of Work signed on December 2, 1986. Since it commenced its full operation in Indonesia in 2000, the company has contributed an estimated total of more than Rp100 trillion in taxes and non-taxes, royalties, salaries, domestic goods and service purchases, and dividends paid to national shareholders. In addition, the company has also conducted social responsibility programs to improve the quality of life and prosperity of the surrounding communities by providing funds in the annual average amount of Rp50 billion. AMNT currently employs approximately 4,000 employees and 3,500 contractors. </p>
						<p class="page-subtitle text-center">*****</p>
					</div>           
                </div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table border="none">
							<tr>
								<td><p class="page-subtitle">Media contact </p></td>
								<td></td>
							</tr>
							<tr>
								<td><p class="page-subtitle">Rubi W. Purnomo</p></td>
								<td><p class="page-subtitle">&ensp; : +62 811 940 399</p></td>
							</tr>
							<tr>
								<td><p class="page-subtitle">Email</p></td>
								<td><p class="page-subtitle">&ensp; : rubi.purnomo@nnt.co.id / rubi.purnomo@amnt.co.id</p></td>
							</tr>
						</table>
                    </div>           
                </div>
				<div class="spacer"><br></div>
            </div>
        </section>

<?php include 'partials/footer.php'; ?>