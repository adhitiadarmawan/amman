<?php include 'partials/header.php'; ?>
		<section id="hero">
			<div class="container-fluid">
				<div class="col-md-6 hero-large" style="background-image: url('images/hero/hero-1.jpg');">
					<h2>Operating more than <br> 110 units of haul trucks. <span></span></h2>
					<a href="javascript:;"></a>
				</div>

				<div class="col-md-6 no-padding">
					<div class="hero-small" style="background-image: url('images/hero/hero-2.jpg');">
						<h2>Concentrator facility at night.</h2>
						<a href="javascript:;"></a>
					</div>
					<div class="hero-small" style="background-image: url('images/hero/hero-3.jpg');">
						<h2>Mining area adjacent to our <br>reclaimed land and pristine forest</h2>
						<a href="javascript:;"></a>
					</div>
					<div class="hero-small" style="background-image: url('images/hero/hero-4.jpg');">
						<h2>Transporting processed copper <br>concentrate at Benete Port</h2>
						<a href="javascript:;"></a>
					</div>
				</div>
			</div>
		</section>


        <section id="about" class="page-section first-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 animation-element slide-left col-sm-pull-right">
                        <h3 class="page-title">Continuously Working to Develop Indonesia</h3>
						<hr>
						<p class="page-subtitle">Thriving as a national corporation with worldwide capability has us assured of being the leading player in the mining industry. Embrace in the highest operating standard and green acuity, as well as putting on the dedication of son and daughters of the nation, let us exert actual work for Indonesia.</p>
						<!--img src="images/button_pelajari.png" class="page-button"-->
						<div class="spacer"></div><br>
						<div class="spacer"></div><br>
					</div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                    </div>
                </div>
            </div>
        </section>


        <section id="gallery" class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 animation-element slide-right">
                        <h3 class="page-title">Employees Safety is Our Priority</h3>
						<hr>
						<p class="page-subtitle">The vision as a valuable and respected firm over the attainment of performance in the mining industry envisages the importance of the highest safety standard. This practice rigorously follows AMNT’s Health, Safety, and Corporate Loss Control Policy.</p>
						<!--img src="images/button_pelajari.png" class="page-button"-->
						<div class="spacer"></div><br>
						<div class="spacer"></div>
					</div>
                </div>
            </div>
        </section>


        <section id="contact" class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 animation-element slide-left">
                        <h3 class="page-title">Environmental Sustainability for the Future</h3>
						<hr>
						<p class="page-subtitle">We pursue innovation to glean the finest solution for a responsible environmental preservation. At the nexus of our heart settles a solid leadership and the application of a proficient, transparent, and sustainable environmental management.</p>
						<!--img src="images/button_pelajari_white.png" class="page-button"-->
						<div class="spacer"></div><br>
						<div class="spacer"></div>
					</div>
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        
                    </div>
                </div>
            </div>
        </section>
        
		<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<div align="center" class="embed-responsive embed-responsive-16by9 desktop">
							<video controls class="embed-responsive-item">
								<source src="http://jkte99.com/amman2/video/testimoni_rahmat_makkasau.mp4" type="video/mp4">
							</video>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="modal fade" id="videoModal2" tabindex="-1" role="dialog" aria-labelledby="videoModal2" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<div class="mobile">
							<iframe width="100%" height="350" src=""></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php include 'partials/footer.php'; ?>